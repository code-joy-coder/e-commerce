<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search Yours</title>
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</head>

<body>
<div class="container">
    <form class="w-50 mt-5" action="<?=base_url( 'e-commerce/Sy_c/insert_cart' )?>" method="post">
        <input class="form-control" type="text" name="id" placeholder="주문자 아이디" required>
        <input class="form-control" type="password" name="pw" placeholder="주문자 비번" required>
        <input class="form-control" type="text" name="product" placeholder="주문 물품" required>
        <!-- <button type="submit">장바구니 삽입</button> -->
        <input class="btn btn-primary" class="form-control" type="submit" value="장바구니 삽입">
    </form>

    <form class="w-50 mt-5" action="<?=base_url( "e-commerce/Sy_c/search_cart" )?>" method="post">
        <input class="form-control" type="text" name="id" placeholder="주문자 아이디" required>
        <input class="form-control" type="password" name="pw" placeholder="주문자 비번" required>
        <button type="submit" class="btn btn-primary">장바구니 검색</button>
    </form>
</div>
<!-- <script src="<?=base_url('src/shpmall/sy_home.js')?>"></script> -->
</body>
</html>