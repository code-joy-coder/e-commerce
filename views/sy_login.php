<? defined('BASEPATH') OR exit('No direct script access allowed');

    echo $this->sy_library->print_header( "Log In" );
    
?>
<section class="login-container">
	<form class="login-form">
		<div class="title">Login</div><br>
		<div>Email</div>
		<input type="email" id="id_login" placeholder=" email@searchyoures.site" required autofocus>
		<div>password</div>
		<input type="password" id="pw_login" placeholder=" ••••••••••" required>
		<button type="button" onclick="logIn();">Log In</button>		
		<button type="button" onclick="logOut();">Log Out</button>		
	</form>		           
	<br><br><br>
	<form class="signup-form">
		<div class="title">Create Your Account</div><br>
		<div>Email</div>
		<input type="email" id="id_signup" placeholder=" email@searchyoures.site" required>
		<div>Password</div>
		<input type="password" id="pw_signup" placeholder=" ••••••••••" required>
		<div>Password(check)</div>
		<input type="password" id="pw_signupRe" placeholder=" ••••••••••" required>
		<button type="button" onclick="signUp();">Sign Up</button>			
	</form>
</section>
        
	<script src="<?=base_url("src/shpmall/sy_home.js")?>"></script>
</body>
</html>
