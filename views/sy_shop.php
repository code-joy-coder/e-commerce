<? defined('BASEPATH') OR exit('No direct script access allowed');

    echo $this->sy_library->print_header( "Shop" );
?>

<section class="product-list">
<?
    for ( $i = 0 ; $i < sizeof($list_product) ; $i++ ){
        if ( $i % 3 == 0 ) echo "<div>";

        echo $this->sy_library->print_product_list( $list_product[$i] );

        if ( $i % 3 == 2 ) echo "</div>";
    }    
?>
</section>


<section class="page-list" style="font-size: 2em;">
	<?
		for ( $i = 1; $i <= $pages; $i++ ){
			if ( $i == $page ) echo "<a class='page-number active' href='".base_url( "e-commerce/sy_c/shop/".$i )."'>".$i."</a>";
			else echo "<a class='page-number' href='".base_url( "e-commerce/sy_c/shop/".$i )."'>".$i."</a>";
		}
	?>
</section>


<script src="<?=base_url('src/shpmall/sy_home.js')?>"></script>
</body>
</html>