<? defined('BASEPATH') OR exit('No direct script access allowed');

    echo $this->sy_library->print_header("Home");
?>

<section class="main-banner">
    <div class="display-01">
        <div class="display-img"></div>
        <span class="display-h1">Admire Stylish Dresses & Looks</span>
        <span class="display-h2">If we wanted to build a human-level tool to offer automated outfit advice, we needed to understand 
    people’s fashion taste. </span>
        <button type="button" class="btn-display-01" onclick="<?=base_url('e-commerce/sy_c/product/DR-01')?>">Show More</button>
    </div>
</section>

<section class="sub-banner">
    <a href="<?=base_url('e-commerce/sy_c/shop')?>" class="sub-01">
        <span class="sub-h1">Women Collection</span>
        <span class="sub-h2">Spring 2021</span>
        <div class="sub-01-label">Popular</div>
    </a>
    <a href="<?=base_url('e-commerce/sy_c/shop')?>" class="sub-nav">
        <span class="sub-h2">24 items</span>
        <span class="sub-h1">Best Sellers</span>
    </a>
    <a href="<?=base_url('e-commerce/sy_c/shop')?>" class="sub-02">
        <span class="sub-h1">Dresses</span>
        <div class="sub-02-label">New</div>
    </a>
    <a href="<?=base_url('e-commerce/sy_c/shop')?>" class="sub-03">
        <span class="sub-h1">Denim Jackets</span>
        <div class="sub-03-label">New</div>
    </a>
</section>

<section class="product-list">
    <div class="title">You may like</div>
    <?
        for ( $i = 0 ; $i < sizeof($list_product) ; $i++ ){
            if ( $i % 3 == 0 ) echo "<div>";
            echo $this->sy_library->print_product_list( $list_product[$i] );
            if ( $i % 3 == 2 ) echo "</div>";
        }
    ?>
</section>

<script src="<?=base_url('src/shpmall/sy_home.js')?>"></script>
</body>
</html>