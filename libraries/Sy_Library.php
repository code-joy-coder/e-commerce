<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Sy_library {
    
    public function print_header ( $page = "" ){
        return 
        '<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Search Yours : '.$page.'</title>
            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
            <link rel="shortcut icon" href="#">
            <script src="'.base_url("src/shpmall/xzoom.min.js").'"></script>
            <link rel="stylesheet" href="'.base_url("src/shpmall/sy.css").'">
        </head>
        
        <body>
        <header>
          <div class="header-container">
            <a href="'.base_url("e-commerce/sy_c/home").'" class="logo"></a>
            <a href="'.base_url("e-commerce/sy_c/login_page").'" class="login">Login</a>
            <a onClick="loadCart();" class="cart"></a>
            <div class="sns">
                <a href=# class="instagram"></a>
                <a href=# class="twitter"></a>
                <a href=# class="facebook"></a>
            </div>
            <nav>
                <div class="Home"><a href="'.base_url("e-commerce/sy_c/home").'">Home</a></div>
                <div class="Shop"><a href="'.base_url("e-commerce/sy_c/shop").'">Shop</a></div>
                <div class="Blog"><a href=#>Blog</a></div>
                <div class="Contact"><a href=#>Contact</a></div>
            </nav>
          </div>
        </header>
        ';
    }

    // public function print_product_list( $list ) {
    //     return 
    //     '<div class="product">
    //         <img class="product_image" src="'.base_url( $list["image"] ).'">
    //         <div class="product_name">'.$list["title"].'</div>
    //         <div class="product_descript">'.$list["descript"].'</div>
    //         <div class="product_price">$'.$list["price"].'</div>
    //         <button type="button" class="btn-sub-list" onclick="location.href=\''.base_url("e-commerce/sy_c/product/").$list["sku"].'\'">Shop Now</button>
    //     </div>';
    // }

    public function print_product_list( $list ) {
        return 
        '<div class="product">
            <img class="product_image" src="'.base_url( "src/shpmall/images/products/" ).$list["image"][0].'">
            <div class="product_name">'.$list["title"].'</div>
            <div class="product_descript">'.$list["descript"].'</div>
            <div class="product_price">$'.$list["price"].'</div>
            <button type="button" class="btn-sub-list" onclick="location.href=\''.base_url("e-commerce/sy_c/product/").$list["sku"].'\'">Shop Now</button>
        </div>';
    }

    public function print_product_info ( $product ) {        
        echo 
        '<section class="container">
            <div class="product-img">
                <img id="img-big" src="">
                <div class="img-big-indicator-left" onclick="changeImg(-1)"></div>
                <div class="img-big-indicator-right" onclick="changeImg(1)"></div>
                <div class="img-thumbs-container">';

        for ( $i = 0 ; $i < sizeof($product["image"]) ; $i++ ){
            echo
            '            <img class="img-thumbs img-small'.$i.'" src="'.base_url("src/shpmall/images/products/").$product["image"][$i].'" onclick="expandImg(this);">';
        }        
        echo
        '       </div>
            </div>
            <div class="product-info">
                <div class="label">'.$product["label"].'</div>
                <div id="product" value="'.$product["sku"].'">'.$product["title"].'</div>
                <div>
                <img src="'.base_url("src/shpmall/images/products/rating_4star.png").'" class="ratings">
                <span class="reviews">'.$product["review"].' reviews</span>
                </div>
                <div class="descript">'.$product["descript"].'</div>
        
                <div class="feature">
                    <div>Size</div>';

        for ( $i = 0 ; $i < sizeof($product["size"]) ; $i++ ){
            echo
            '       <button class="feature-size" value="'.$product["size"][$i].'" onclick="selected(this);">'.$product["size"][$i].'</button>';
        }
        echo
        '       </div>
                
                <div class="feature">
                    <div>Color</div>';

        for ( $i = 0 ; $i < sizeof($product["color"]) ; $i++ ){
            echo
            '        <button class="feature-color '.$product["color"][$i].'" value="'.$product["color"][$i].'" onclick="selected(this);"></button>';
        }
        echo        
        '        </div>
        
                <div class="feature">
                    <div>Quantity</div>
                    <button class="feature-quantity" onclick="changeCount(-1);">-</button>
                    <span id="orderQuantity"></span>
                    <button class="feature-quantity" onclick="changeCount(1);">+</button>
        
                </div>
        
                <div class="payment-container">
                    <div id="price">$'.$product["price"].'</div>
                    <button class="purchase">Shop Now</button>
                    <button class="addCart" onclick="add_cart();">Add to cart</button>
                </div>
            </div>
        </section>
        
            <script src="'.base_url("src/shpmall/sy_home.js").'"></script>
        </body>
        </html>        
        ';
    }
    
}
?>