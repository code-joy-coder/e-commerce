<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Model_tools_ecommerce extends CI_Model {
	
	public function get_member( $id ) {
		$this->db->where( "id", $id );
				
		$this->db->limit( 1 );
		
		$result = $this->db->get( "ecommerce_account" );
		
		if ( $result->num_rows() == 0 ) return false;

		return $result->row_array();
	}
    // 1개의 매소드로 '회원가입', '장바구니 추가' 등 DB 추가
    public function insert_DB ( $table, $data ){
        return $this->db->insert( $table, $data );
    }
	// 장바구니 조회
	public function search( $account_no ) {
		$this->db->select( "ecommerce_cart.*, ecommerce_product.product_sku, ecommerce_product.title, ecommerce_product.price, ecommerce_product.image");
		$this->db->where( "account_no", $account_no );
		$this->db->from("ecommerce_cart");
		$this->db->join("ecommerce_product", "ecommerce_product.product_sku = ecommerce_cart.product_sku","inner");				
		return $this->db->get();
	}

}

?>