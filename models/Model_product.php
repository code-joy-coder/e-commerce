<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Model_product extends CI_Model {
  
  public function product_info ( $sku ){
    $this->db->where( "product_sku", $sku );

    return $this->db->get( "ecommerce_product" );
  }
 
  // 제품 리스트 출력
  public function product_list ( $condition = null, $page, $num ){

    //전체 갯수를 뽑아내어 필요한 페이지수 계산	
    if ( $condition === null ) {
        $result = $this->db->get( "ecommerce_product" );
		
    } else {
        $result = $this->db->get_where( "ecommerce_product", $condition );
    }

	$pages = ceil( $result->num_rows() / $num );

	//페이지에 해당하는 품목들 검색
	$this->db->limit( $num, ( $page - 1 ) * $num );
    
    if ( $condition === null ) {
        $result = $this->db->get( "ecommerce_product" );
		
    } else {
        $result = $this->db->get_where( "ecommerce_product", $condition );
    }

	//테이블에서 뽑아온 데이터들을 $products 에 이차원 배열로 삽입
	$products = array();

    foreach ( $result->result_array() as $rows ) {
        $data = array(
            "sku"      => $rows["product_sku"],
            "title"    => $rows["title"],
            "size"     => json_decode($rows["size"]),
            "color"    => json_decode($rows["color"]),
            "price"    => $rows["price"],
            "review"   => $rows["review"],
            "rating"   => $rows["rating"],
            "image"    => json_decode($rows["image"]),
            "label"    => $rows["label"],
            "descript" => $rows["descript"]
        );

        array_push($products, $data); // product 배열에 제품정보 담긴 배열 추가
    }

	$total_data = array(
		"pages"    => $pages,
		"products" => $products
	);

    return $total_data;
  }
  
  public function main_list() {
    $data = array(
        array(
            "sku" => "Suite-01",
            "image" => ["Yves_Saint_Laurent.png"],
            "title" => "Women's costume Yves Saint Laurent",
            "descript" => "Fashion is something we deal with everyday. Even people who say t…",
            "price" => "1140"
        ),
        array(
            "sku" => "HT-01",
            "image" => ["Mens_black_hat.png"],
            "title" => "Men’s black hat Gucci",
            "descript" => "The fedora hat is reimagined for Pre-Fall 2020 with a maxi embroidered label de...",
            "price" => "540"
        ),
        array(
            "sku" => "DR-02",
            "image" => ["Dres_Karl_Lagerfeld.png"],
            "title" => "Dress Karl Lagerfeld",
            "descript" => "Another masterpiece from the legendary designer. He designed new dress dev…",
            "price" => "640"
        ),
        array(
            "sku" => "TI-01",
            "image" => ["Mens_black_tie_Valentino.png"],
            "title" => "Men’s black tie Valentino",
            "descript" => "Jacquard Valentino tie with Valentino logo motif.",
            "price" => "225"
        ),
        array(
            "sku" => "JK-01",
            "image" => ["Mens_jacket_black.png"],
            "title" => "Men’s jacket Off- white",
            "descript" => "Off-White scuffed denim jacket. Faded effect, baggy cut, pointed collar, clasp…",
            "price" => "215"
        ),
        array(
            "sku" => "BS-01",
            "image" => ["Cotton_blouse.png"],
            "title" => "Cotton blouse Dries van noten",
            "descript" => "The fedora hat is reimagined for Pre-Fall 2020 with a maxi embroidered label de...",
            "price" => "180"
        )
    );
    return $data;
  } 

}

?>