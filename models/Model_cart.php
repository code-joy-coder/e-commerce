<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Model_cart extends CI_Model {
	
	public function insert( $data ) {
		return $this->db->insert( "sy_cart", $data );
	}

	public function get_member( $id ) {
		$this->db->where( "id", $id );
				
		$this->db->limit( 1 );
		
		$result = $this->db->get( "sy_account" );
		
		if ( $result->num_rows() == 0 ) return false;

		return $result->row_array();
	}

	public function search( $account_no ) {
		$this->db->where( "account_no", $account_no );
		
		return $this->db->get( "sy_cart" );
	}

}

?>