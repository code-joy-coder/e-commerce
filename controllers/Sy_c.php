<? if (!defined('BASEPATH')) exit('No direct script access allowed');

/* application/controllers/Sy_c.php */

  class Sy_c extends CI_Controller
  {
    // 메인화면
    public function Home()
    {
      $this->load->library( "Sy_Library" );
      $this->load->model( "e-commerce/Model_product" );
      $data = array(
        "list_product" => $this->Model_product->main_list()
      );
      $this->load->view( "e-commerce/SY_home", $data );
    }

    // Shop 화면
	public function Shop( $page = 1 )
	{
		$this->load->library( "Sy_Library" );
		$this->load->model( "e-commerce/Model_product" );
		$num = 6;
		$total_data = $this->Model_product->product_list( null, $page, $num );
		$data = array(
			"page"         => $page,
			"pages"        => $total_data["pages"],
			"list_product" => $total_data["products"]
		);

		$this->load->view( "e-commerce/sy_shop", $data );
	}

    public function Shop_search( $condition = null )
    {
      $this->load->model( "e-commerce/Model_product" );
      $products = $this->Model_product->product_list( $condition );
    }

    // 로그인 페이지
    public function Login_page()
    {
      $this->load->library( "Sy_Library" );
      $this->load->view( "e-commerce/sy_login" );
    }
    
    // 회원가입 
    public function Signup()
    {
      $this->load->library( "form_validation" );
      $this->load->model( "e-commerce/Model_tools_ecommerce" );

      $this->form_validation->set_rules( "id", "", "required" );
      $this->form_validation->set_rules( "pw", "", "required" );

      if ( !$this->form_validation->run() ){
        echo "(Server) 빈칸 다 채우주세요~";
        return false;
      }
      
      if ( $account = $this->Model_tools_ecommerce->get_member( $_POST["id"] ) ){
        echo "이미 존재하는 아이디 입니다.";
        return false;
      }

      $data = array (
        "id" => $_POST["id"],
        "pw" => $_POST["pw"]
      );

      if ( $this->Model_tools_ecommerce->insert_DB( "ecommerce_account", $data ) ) echo "회원가입 성공!";
      else echo "회원가입 실패!";
    }
    
    // 로그인 
    public function Login()
    {
      $this->load->library( "form_validation" );
      $this->load->model( "e-commerce/Model_tools_ecommerce" );

      $this->form_validation->set_rules( "id", "", "required" );
      $this->form_validation->set_rules( "pw", "", "required" );

      if ( !$this->form_validation->run() ){
        echo "(Server) 빈칸 다 채우주세요~";
        return false;
      }
      
      if ( !$account = $this->Model_tools_ecommerce->get_member( $_POST["id"] ) ){
        echo "아이디가 존재하지 않습니다.";
        return false;
      }

      if ( $account["pw"] != $_POST["pw"]){
        echo "비밀번호가 틀렸습니다.";
        return false;
      }
      
      $this->session->set_userdata( "user_session", $account["account_no"] );

      if ( isset($_SESSION["user_session"]) ) echo "로그인 성공!";
      else echo "로그인 실패!";

    }

    // 로그아웃
    public function Logout()
    {
      if ( isset($_SESSION["user_session"]) ) {
        unset( $_SESSION["user_session"] );
        echo "로그아웃 성공!";
      }
      else echo "로그인 되어 있지 않습니다.";
    }

    // 제품 상세 페이지
    public function Product( $product_sku )
    {
      $this->load->library( "Sy_Library" );
      $this->load->model( "e-commerce/Model_product" );
      $result = $this->Model_product->product_info( $product_sku );
      foreach ($result->result_array() as $rows)
      {
        $data = array(
          "product" => array(
              "sku"      => $rows["product_sku"],
              "title"    => $rows["title"],
              "size"     => json_decode($rows["size"]),
              "color"    => json_decode($rows["color"]),
              "price"    => $rows["price"],
              "review"   => $rows["review"],
              "rating"   => $rows["rating"],
              "image"    => json_decode($rows["image"]),
              "label"    => $rows["label"],
              "descript" => $rows["descript"]
          )
        );
      } 
      
      $this->load->view( "e-commerce/sy_product", $data );
    }

    // 장바구니 검색
    public function cart() {      
      if ( !isset($_SESSION["user_session"]) ) {
        echo "로그인 되어 있지 않습니다.";
        return false;
      }
      
      $this->load->model( "e-commerce/Model_tools_ecommerce" );

      $List = $this->Model_tools_ecommerce->search( $_SESSION["user_session"] );
      
      $i = 1;
      $total_price = 0;
      if ( $List->num_rows() == 0 ) echo "장바구니가 비었습니다!";
      else {        
        echo
        "
        <section class='cart-container'>
          <div class='payment-process'>01. Cart</div>
          <div class='payment-process'>02. Checkout</div>
          <div class='payment-process'>03. Shipping</div>
          <div class='payment-process'>04. Done</div>
          <div class='line-gray'><div class='line-active'></div></div>
        ";
        foreach ( $List->result_array() as $rows ){
          echo
          "
            <div>
              <img class='cart-img' src='".base_url("src/shpmall/images/products/").json_decode($rows["image"])[0]."'>
              <div>
                <div class='cart-title'>".$rows["title"]."</div>
                <div>size: <span class='cart-size'>".$rows["size"]."</span></div>
                <div>color: <span class='cart-color'>".$rows["color"]."</span></div>            
              </div>
              <div>
                <button class='cart-quantity' onclick='cart-count(-1);'>-</button>
                <span class='cart-quantity-show'>".$rows["quantity"]."</span>
                <button class='cart-quantity' onclick='cart-count(+1);'>+</button>
              </div>
              <div class='cart-price'>$
                <span class='cart-price-show'>".$rows["price"]."</span>
              </div>
            </div>
          ";
          $total_price += (int)$rows["quantity"] * (int)$rows["price"];
          $i++;
        }
        echo
            "
            </section>
            <div class='totalPrice'>
              <span>Total amount</span>
              <span>$".$total_price."</span>
            </div>
            ";
        }
    }

    //장바구니 추가
    public function insert_cart(){      
      if ( !isset($_SESSION["user_session"]) ) {
        echo "로그인 되어 있지 않습니다.";
        return false;
      }

      $this->load->library( "form_validation" );
      $this->form_validation->set_rules( "sku", "", "required" );
      $this->form_validation->set_rules( "size", "", "required" );
      $this->form_validation->set_rules( "color", "", "required" );
      $this->form_validation->set_rules( "quantity", "", "required" );
      
      if ( !$this->form_validation->run() ){
        echo "(Server) Need more order information";
        return false;
      }
      
      $this->load->model( "e-commerce/Model_tools_ecommerce" );
      $data = array(
        "account_no"  => $_SESSION["user_session"],
        "product_sku" => $_POST["sku"],
        "size"        => $_POST["size"],
        "color"       => $_POST["color"],
        "quantity"    => $_POST["quantity"]
      );
      
      if ( $this->Model_tools_ecommerce->insert_DB( "ecommerce_cart", $data ) ) echo "장바구니 추가 성공!";
      else echo "장바구니 추가 실패!";
    }

  }
?>
