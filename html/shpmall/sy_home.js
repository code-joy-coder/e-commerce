// Set Oder Info
let order_info = { "product_sku":"","size":"","color":"","quantity":0,"unit price":0 };

// Add to Cart
function add_cart(){
  let checkSelect = document.getElementsByClassName("selected");
  if( checkSelect.length < 2) {
    alert("Need to select All options");
  }else{
    order_info["product_sku"] = document.getElementById("product").getAttribute("value");
    order_info["size"] = checkSelect[0].value;
    order_info["color"] = checkSelect[1].value;
    order_info["quantity"] = parseInt(document.getElementById("orderQuantity").textContent,10);
    order_info["unit price"] = parseInt(document.getElementById("price").textContent.split("").slice(1).join(""), 10);
    console.log(order_info);
    $.post("https://searchyours.site/e-commerce/Sy_c/insert_cart",
        {
        sku:order_info["product_sku"],
        size:order_info["size"],
        color:order_info["color"],
        quantity:order_info["quantity"],
        price:order_info["price"]
        },
        function (data){
            alert(data);
        }
    );
  }
}

// Selected Button CSS Apply
function selected ( obj ) {
  let parent = obj.parentNode;
  if (parent.querySelector(".selected")) parent.querySelector(".selected").classList.remove("selected");
  obj.classList.add("selected");
}

// Product Main img Slide
let slideIndex = 0;
showImg(slideIndex);

function changeImg( n ) {
  showImg(slideIndex += n);
}

function showImg( n ) {
  let x = document.getElementsByClassName("img-thumbs");
  if (n > x.length-1) {slideIndex = 0}
  if (n < 0) {slideIndex = x.length-1} ;
  document.getElementById("img-big").setAttribute( 'src', x[slideIndex].attributes.src.value );
  document.getElementById("img-big").setAttribute( 'xoriginal', x[slideIndex].attributes.src.value );
}

// Image Preivew (expand)
$('#img-big').xzoom({zoomWidth: 500, tint: '#333', Xoffset: 30});

// Thumbs click
function expandImg( obj ){
  document.getElementById("img-big").setAttribute( 'src', obj.src );
  document.getElementById("img-big").setAttribute( 'xoriginal', obj.src );
  // slideIndex 업데이트
  let x = document.getElementsByClassName("img-thumbs");
  let key = obj.classList;
  slideIndex = Object.entries(x).findIndex(arr => arr[1]["classList"] === key);  
}

// Order Quantity
let orderCount = 1;
showCount(orderCount);

function changeCount( n ) {
  showCount(orderCount += n);
}

function showCount ( n ) {
  if ( n < 1 ) {orderCount = 1}
  document.getElementById("orderQuantity").textContent = orderCount;
}


// Log In 
function logIn(){
  let sendId = document.getElementById("id_login").value;
  let sendPw = document.getElementById("pw_login").value;
  $.post("https://searchyours.site/e-commerce/sy_c/login",
          { id:sendId, pw:sendPw },
          function (data){
            alert(data);
            document.getElementById("id_login").value = "";
            document.getElementById("pw_login").value = "";
          }
  );
}

// Sign Up
function signUp(){
  let sendId = document.getElementById("id_signup").value;
  let sendPw = document.getElementById("pw_signup").value;
  let sendPwRe = document.getElementById("pw_signupRe").value;
  if ( sendPw !== sendPwRe){
    alert("두 비밀번호가 일치하지 않습니다.");
    return false;
  }
  $.post("https://searchyours.site/e-commerce/sy_c/signup",
          { id:sendId, pw:sendPw },
          function (data){
            alert(data);
            document.getElementById("id_signup").value = "";
            document.getElementById("pw_signup").value = "";
            document.getElementById("pw_signupRe").value = "";
          }
  );
}

// Log Out
function logOut() {
  $.post("https://searchyours.site/e-commerce/sy_c/logout",
          {},
          function (data){
            alert(data);
          }
  );
}

// Load Cart
function loadCart() {
  $.post("https://searchyours.site/e-commerce/sy_c/cart",
          {},
          function (data){
            if ( data == "로그인 되어 있지 않습니다." || data == "장바구니가 비었습니다!"){
              alert(data);
            }else{
              $("header").nextAll().replaceWith( data );
            };
          }
  );
}

